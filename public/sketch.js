var socket;

function setup() {
    createCanvas(600, 600);
    background(51);
    socket = io.connect();
    socket.on("mouse", function(data) {
        noStroke();
        fill(255, 0, 100);
        ellipse(data.x, data.y, 20, 20);
    });

}

function mouseDragged() {
    console.log("Envoi : " + mouseX + "," + mouseY);

    var data = {
        x: mouseX,
        y: mouseY
    }

    socket.emit("mouse", data);

    noStroke();
    fill(255);
    ellipse(mouseX, mouseY, 20, 20);
}
